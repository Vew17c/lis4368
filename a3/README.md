> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Assignment 3 Requirements: 

1. Generate the ERD
2. Forward Engineer the data to local database
3. Populate the database with 10 records.

README.md file should include the following items:        

* Screenshot ERD
* Workbench file of the database
* SQL file of the database data
                                                                                                                    
Assignment Screenshots: 

*Screenshot of ERD*:

![JDK Installation Screenshot](../img/ERD.png)

*Link to A3 Workbench File*:

[A3 Database Workbench File](a3_data_work.mwb)

*Link to A3 SQL File*:

[A3 Database SQL](a3_database.sql)