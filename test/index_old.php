<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Your Name Here!">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4381 - Assignment4</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("global/nav.php");?>
	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Preform Calculation</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Number 1:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="numb1" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Number 2:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="numb2" />
										</div>
								</div>

							<input type="radio" name="option" id="add" value="add" checked="true"> Addition &nbsp;&nbsp;
							<input type="radio" name="option" id="subtract" value="subtract"> Subtraction &nbsp;&nbsp;
							<input type="radio" name="option" id="multiply" value="multiply"> Multiplcation &nbsp;&nbsp;
							<input type="radio" name="option" id="divide" value="divide"> Division &nbsp;&nbsp;
							<input type="radio" name="option" id="exponate" value="exponate"> Exponetiate </p>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="calculate" value="Calculate" onclick="calNumber()">Calculate</button>
										</div>
								</div>
						</form>
			
			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					numb1: {
							validators: {
									notEmpty: {
											message: 'Number Required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Must be a whole number'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Can only contain numbers'
									},									
							},
					},

					numb2: {
							validators: {
									notEmpty: {
											message: 'Number Required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Must be a whole number'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,	
									message: 'Can only contain numbers'
									},									
							},
					},
					
			}
	});
});

</script>

</body>
</html>