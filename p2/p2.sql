-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `vew17c` ;

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vew17c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `vew17c` ;

-- -----------------------------------------------------
-- Table `vew17c`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT(9) UNSIGNED NOT NULL,
  `cus_phone` INT(10) UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(8,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`),
  UNIQUE INDEX `per_id_UNIQUE` (`cus_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `pst_name` VARCHAR(45) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` INT(10) UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(8,2) NOT NULL,
  `pst_notes` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`pst_id`, `pst_notes`),
  UNIQUE INDEX `per_id_UNIQUE` (`pst_id` ASC))
ENGINE = InnoDB
COMMENT = '		';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `str_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(30) NOT NULL,
  `pet_cost` DECIMAL(5,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(20) NOT NULL,
  `pet_sales_date` DATE NOT NULL,
  `pet_vaccine` ENUM('Y', 'N') NOT NULL,
  `pet_neuter` ENUM('Y', 'N') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  UNIQUE INDEX `pet_id_UNIQUE` (`pet_id` ASC),
  INDEX `fk_pet_store_idx` (`str_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_store`
    FOREIGN KEY (`str_id`)
    REFERENCES `vew17c`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `vew17c`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `vew17c`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Wilber', 'Fische', '5749 Dayflower Circle', 'Tallahassee', 'FL', 32354, 8505451934, 'wfische@gmail.com', 500.00, 10000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'John', 'Travolta', '7906 Bradford Street', 'Tallahassee', 'FL', 32355, 8505551834, 'jtravolta@gmail.com', 1000.00, 15000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Andy', 'Griffin', '782 Spruce Street ', 'Tallahassee', 'FL', 32356, 8505651734, 'agriffin@gmail.com', 1500.00, 20000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Luke', 'Skywalker', '8906 Center Ave. ', 'Tallahassee', 'FL', 32357, 8505751634, 'lskywalker@gmail.com', 2000.00, 25000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Andrew', 'Jackson', '11 Princess Ave. ', 'Tallahassee', 'FL', 32358, 8505851534, 'ajackson@gmail.com', 2500.00, 30000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Taylor', 'Hacker', '347 Lakeshore Rd. ', 'Tallahassee', 'FL', 32359, 8505951434, 'thacker@gmail.com', 3000.00, 35000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Connor', 'McGiven', '54 Clay Drive ', 'Tallahassee', 'FL', 32360, 8505051234, 'cmcgiven@gmail.com', 3500.00, 40000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Evan', 'Powell', '86 NE. Clay Street ', 'Tallahassee', 'FL', 32361, 8505151334, 'epowell@gmail.com', 4000.00, 45000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Vincent', 'Willson', '310 Proctor St. ', 'Tallahassee', 'FL', 32362, 8505251234, 'vwillson@gmail.com', 4500.00, 50000.00, NULL);
INSERT INTO `vew17c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Jeffery', 'Pattersen', '67 Penn Dr. ', 'Tallahassee', 'FL', 32363, 8505351134, 'jpattersen@gmail.com', 5000.00, 55000.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'PetCo', '15972 Mahan Drive', 'Tallahassee', 'FL', 32311, 8501231234, 'petco@mail.net', 'dummy1@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'WeLoveAnimals', '15972 Mahan Drive', 'Tallahassee', 'FL', 32312, 8504125689, 'weloveanimals@mail.net', 'dummy2@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'HomesForU', '15972 Mahan Drive', 'Tallahassee', 'FL', 32313, 8504123694, 'homes4u@mail.net', 'dummy3@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Animal Shelter', '15972 Mahan Drive', 'Tallahassee', 'FL', 32314, 8504789652, 'animalshelter@mail.net', 'dummy4@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'PuppyLove', '15972 Mahan Drive', 'Tallahassee', 'FL', 32315, 8501236547, 'puppylove@mail.net', 'dummy5@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'NewLife', '15972 Mahan Drive', 'Tallahassee', 'FL', 32316, 8506958741, 'newlife@mail.net', 'dummy6@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Animal House', '15972 Mahan Drive', 'Tallahassee', 'FL', 32317, 8502135684, 'ahouse@mail.net', 'dummy7@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Tallahassee Shelter', '15972 Mahan Drive', 'Tallahassee', 'FL', 32318, 8504236541, 'tallyShelter@mail.net', 'dummy8@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Protect Life', '15972 Mahan Drive', 'Tallahassee', 'FL', 32319, 8502122231, 'pLife@mail.net', 'dummy9@gmail.com', 85000.80, DEFAULT);
INSERT INTO `vew17c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Cats and Dogs', '15972 Mahan Drive', 'Tallahassee', 'FL', 32320, 8504568996, 'catdog@mail.net', 'dummy10@gmail.com', 85000.80, DEFAULT);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 1, 'Labrador Retriver', 80.00, 1, 'Brown', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 2, 'Black Labrador', 50.00, 2, 'Black', '2017-04-07', 'N', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 3, 'Golden Retriver', 78.00, 3, 'Gold', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 4, 'German Sheppard', 56.64, 1, 'Brown', '2017-04-07', 'N', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 5, 'Boxer', 89.21, 5, 'Brown', '2017-04-07', 'N', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 6, 'Beagles', 55.63, 2, 'Brown', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 7, 'Corgi', 41.25, 6, 'Gold', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 8, 'Pug', 45.69, 1, 'Brown', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 9, 'Yorkshire Terrier', 96.21, 2, 'Brown', '2017-04-07', 'Y', 'N', NULL);
INSERT INTO `vew17c`.`pet` (`pet_id`, `str_id`, `cus_id`, `pet_type`, `pet_cost`, `pet_age`, `pet_color`, `pet_sales_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 10, 'Doberman', 56.21, 3, 'Brown', '2017-04-07', 'Y', 'N', NULL);

COMMIT;

