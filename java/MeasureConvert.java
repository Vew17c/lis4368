 import java.util.Scanner;
 import java.util.Locale;
 import java.text.NumberFormat;

 public class MeasureConvert {
	public static void main(String[] args) {
		
        int inches = 0;
        double inToCm = 0.0,
               inToM  = 0.0,
               inToFt = 0.0,
               inToYd = 0.0,
               ftToMi = 0.0,
               testInp = 0.0;
        boolean ctrl = false;
        String inpVar = "";
        Scanner input = new Scanner(System.in);

        double X = 0; 

        do { // This loop allows the program to continuously prompt the user for input, as long as the user types in inccorect input.
            do { //This loop is the actual code that gathers the input from the user.
                System.out.print("Type in an integer: ");
                inpVar = input.next();

                try {
                    testInp = Double.parseDouble(inpVar);
                } catch (Exception e) {
                    System.out.print("Input was not a number!\n\n");
                    break;
                }
            
                testInp = testInp % 1;
                if ( testInp != 0.0){
                    System.out.print("Input was not an integer!\n\n");
                    break;
                }

                ctrl = true;

            } while ( testInp != 0 );
        } while ( ctrl == false );

        inches = Integer.parseInt(inpVar);
        inToCm = inches * 2.540000;
        inToM  = inches * 0.025400;
        inToFt = inches / 12;
        inToYd = inches / 36;
        ftToMi = inToFt / 5280;

        //System.out.print(inches + " inch(es) equals:\n\n");

        System.out.printf("%,d" + " inch(es) equal:\n\n" , inches);
        System.out.printf("%,.6f" + " centimeter(s)\n" , inToCm);
        System.out.printf("%,.6f" + " meter(s)\n" , inToM);
        System.out.printf("%,.6f" + " feet\n" , inToFt);
        System.out.printf("%,.6f" + " yard(s)\n" , inToYd);
        System.out.printf("%,.8f" + " mile(s)" , ftToMi);
        System.out.println("\n");

        //System.out.print("End Program");
               
	}
}