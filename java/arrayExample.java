 import java.util.Scanner;
 import java.util.Locale;
 import java.text.NumberFormat;

 public class arrayExample {
	public static void main(String[] args) {
	
        String arr1[] = { "c+" , "c" , "Java" , "Python" , "JSON"};
        String arr2[] = new String[5];
        
        for ( int i = 0 ; i <= 4 ; i++ ){
            arr2[i] = arr1[i];
        }

        System.out.println("Using regular For loop");
        for ( int i = 0 ; i <= 4 ; i++ ){
            System.out.println(arr1[i]);
        }
        
        System.out.println("\n" + "Using enhanced For loop");
        for ( String element : arr2 ){
            System.out.println(element);
        }

        System.out.print("End Program");
	}
}