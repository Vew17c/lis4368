

public class ArrayDemo {
    public static void main (String[] args){
        System.out.print("\n\nProgram creates an array with the following numbers,\nand performs the operations below using user-defined methods. \n\n");

        int arr1[] = { 12 , 15 , 34 , 67 , 4 , 9 , 10 , 7 , 13 , 50};
        int arr2[] = new int[10];
        int arr3[] = new int[10];

        int total = 0,
            r = 0,
            avg = 0;
        double average = 0.0;

        for ( int i = 0 ; i < arr1.length ; i++ ){
            int x = (9 - i);
            arr2[i] = arr1[x];
        }

        for ( int i = 0 ; i < arr1.length ; i++ ){
            total += arr1[i];
        }

        average = ((double)total / arr1.length); 

        System.out.println("Numbers in the array are: " + arr1[0] + " , " + arr1[1] + " , " + arr1[2] + " , " + arr1[3] + " , " + arr1[4] + " , " + arr1[5] + " , " + arr1[6] + " , " + arr1[7] + " , " + arr1[8] + " , " + arr1[9] );     
        System.out.println("Numbers in reverse order are: " + arr2[0] + " , " + arr2[1] + " , " + arr2[2] + " , " + arr2[3] + " , " + arr2[4] + " , " + arr2[5] + " , " + arr2[6] + " , " + arr2[7] + " , " + arr2[8] + " , " + arr2[9] );    
        System.out.println("Sum of all numbers is: " + total);
        System.out.println("Average is: " +  average);

        for ( int i = 0 ; i < arr1.length ; i++ ) {
            if (average < (double)arr1[i]){
                System.out.print(arr1[i] + " ");
                r += 1;

            }
        }

        System.out.println("are greater than the average");
    }
}