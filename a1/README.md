> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

| Assignment 1 Requirements:   | README.md file should include the following items: | Git commands w/short descriptions:
|:--                           |:--                                 |:--
| 1. Distributed Control Setup | * Screenshot of Tomcat running     | 1. git init: Create an empty Git repository or reinitialize an existing one.
| 2. Development Instillations | * Screenshot of Java "Hello World" | 2. git status: Show the working tree status.
| 3. Questions                 | * Git commands                     | 3. git add: Add file contents to the index.
|                              | * Tomcat server links              | 4. git commit: Saves the changes to the repository.
|                              |                                    | 5. git push: Updates remote refs along with associated objects.
|                              |                                    | 6. git pull: Fetch from / integrate with another repository / a local branch.
|                              |                                    | git config: Configures the author's name and email to your commits.
| 

## Assignment Screenshots:

*Screenshot of running java and Hello World*:

![JDK Installations Screenshot](../img/javains.png)

![JDK Hello Screenshot](../img/hello.png)

*Screenshot of Tomcat running http://localhost*:

![Tomcat Screenshot](../img/Tomcat.png)

*Tomcat Homepage*:(http://localhost/lis4368/)

![Homepage](../img/TomcatHome.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)