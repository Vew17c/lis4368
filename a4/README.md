> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Assignment 4 Requirements:

1. Include client side validation
2. Create server side validation
3. Learn about the use of Model Controller View
4. Use JavaBeans for server side validation


README.md file should include the following items:        

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running application on A4 Page
3. Screenshot of Server Side Data running.                                                                                                                   
### Assignment Screenshots:  

*Screenshot of A4 application page*:
![Screenshot of Web App](../img/a4_screen_1.png) 

*Screenshot of valid input*:
![Screenshot of Server SIde Validation working](../img/a4_screen_2.png) 


