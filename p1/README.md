> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Project 1 Requirements: 

1. Create A Database
2. Complete Client Side Validation
3. Learn to create and operate a carousel

README.md file should include the following items:        

* Screenshot Home Pgae
* Screenshot of Customer Table Fields
* Screenshot of Data Validation in work
                                                                                                                    
Assignment Screenshots: 

*Screenshot of Homepage*:

![JDK Installation Screenshot](../img/p1_1.png)

*Screenshot of Project 1*:

![A3 Database Workbench File](../img/p1_2.png)

*Screenshot of Data Validation*:

![A3 Database SQL](../img/p1_3.png)